# zebclient-viya4

## ZebClient - Viya4 integration

### Deploying your cluster


#### With ZebClient in converged deploy mode

* Copy the file `examples/sample-input-zebclient-converged.tfvars` as `zebclient-converged.tfvars`
* Set the required variables
   - subscription_id
   - tenant_id
   - prefix
   - location
   - default_public_access_cidrs
   - ssh_public_key
   - zebclient_license_key

   
<details>
<summary markdown="span">Example for `zebclient-converged.tfvars` file:</summary>
```
# !NOTE! - These are only a subset of CONFIG-VARS.md provided as examples.
# Customize this file to add any variables from 'CONFIG-VARS.md' whose default
# values you want to change.

subscription_id = "XXXXX" #your azure subscription
tenant_id       = "XXXXX" #your azure tenant id

# ****************  REQUIRED VARIABLES  ****************
# These required variables' values MUST be provided by the User
prefix   = "zebclient-converged" # this is a prefix that you assign for the resources to be created
location = "eastus2"             # e.g., "eastus2"
# ****************  REQUIRED VARIABLES  ****************

# !NOTE! - Without specifying your CIDR block access rules, ingress traffic
#          to your cluster will be blocked by default.

# **************  RECOMMENDED  VARIABLES  ***************
default_public_access_cidrs = ["10.0.0.0/32"]
ssh_public_key              = "~/.ssh/viya4_converged.pub"
# **************  RECOMMENDED  VARIABLES  ***************

# Tags can be specified matching your tagging strategy.
tags = {} # for example: { "owner|email" = "<you>@<domain>.<com>", "key1" = "value1", "key2" = "value2" }

# Postgres config - By having this entry a database server is created. If you do not
#                   need an external database server remove the 'postgres_servers'
#                   block below.
postgres_servers = {
  default = {},
}

# Azure Container Registry config
create_container_registry        = false
container_registry_sku           = "Standard"
container_registry_admin_enabled = false

# AKS config
kubernetes_version         = "1.26"
default_nodepool_min_nodes = 2
default_nodepool_vm_type   = "Standard_D8s_v4"

# AKS Node Pools config
node_pools = {
  cas = {
    "machine_type" = "Standard_L32s_v3"
    "os_disk_size" = 200
    "min_nodes"    = 1
    "max_nodes"    = 1
    "max_pods"     = 110
    "node_taints"  = ["workload.sas.com/class=cas:NoSchedule"]
    "node_labels" = {
      "workload.sas.com/class" = "cas"
    }
  },
  compute = {
    "machine_type" = "Standard_L8s_v3"
    "os_disk_size" = 200
    "min_nodes"    = 1
    "max_nodes"    = 1
    "max_pods"     = 110
    "node_taints"  = ["workload.sas.com/class=compute:NoSchedule"]
    "node_labels" = {
      "workload.sas.com/class"        = "compute"
      "launcher.sas.com/prepullImage" = "sas-programming-environment"
    }
  },
  stateless = {
    "machine_type" = "Standard_L16s_v3"
    "os_disk_size" = 200
    "min_nodes"    = 1
    "max_nodes"    = 1
    "max_pods"     = 110
    "node_taints"  = ["workload.sas.com/class=stateless:NoSchedule"]
    "node_labels" = {
      "workload.sas.com/class" = "stateless"
    }
  },
  stateful = {
    "machine_type" = "Standard_L16s_v3"
    "os_disk_size" = 200
    "min_nodes"    = 1
    "max_nodes"    = 3
    "max_pods"     = 110
    "node_taints"  = ["workload.sas.com/class=stateful:NoSchedule"]
    "node_labels" = {
      "workload.sas.com/class" = "stateful"
    }
  }
}

# Jump Box
create_jump_public_ip = true
jump_vm_admin         = "jumpuser"

# Storage for Viya Compute Services
# Supported storage_type values
#    "standard"  - Custom managed NFS Server VM and disks
#    "ha"        - Azure NetApp Files managed service
#    "zebclient" - Zebware storage
storage_type = "zebclient"
# required ONLY when storage_type = zebclient
zebclient_license_key = "A_VALID_LICENSE_KEY"
zebclient_deploy_mode = "converged"
   ```

* Create your cluster using the updated variables file:
   ```bash
   terraform plan -var-file="zebclient-converged.tfvars"
   terraform apply -var-file="zebclient-converged.tfvars"
   ```

</details>

### After Cluster creation

Get access to your cluster by getting the access from the outputs in terraform:

K8S
```bash
terraform output -raw kube_config > ~/.kube/zebclient_converged
export KUBECONFIG=~/.kube/zebclient_converged

#Test config
kubectl get nodes
```

ZebClient Management Node
```bash
# ZebClient Management Node:
zc_mgmt_host=$(terraform output -raw zebclient_converged_management_host)
zc_mgmt_user=$(terraform output -raw zebclient_converged_management_host_user)
terraform output -raw zebclient_converged_management_ssh_key > ~/.ssh/zebclient_converged
chmod 600 ~/.ssh/zebclient_converged
ssh -i ~/.ssh/zebclient_converged $zc_mgmt_user@$zc_mgmt_host
```

In order to install ZebClient agent on any new vm i.e. a SAS viya4 jumpbox, follow the steps below:

- Once the deployment is completed, ssh to the ZebClient management node.
- Run the `zc-cli` tool.
- Choose option `2) Add ZebClient agent node` and then follow instructions in the interactive session.
